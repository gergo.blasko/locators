using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace locators;

[TestFixture]
class Tests
{
    IWebDriver? webDriver;
    string url = "https://www.bbc.com/sport";
    [SetUp]
    public void Init()
    {
        webDriver = new ChromeDriver();
        webDriver.Manage().Window.Maximize();
    }

    [Test]
    public void Test_For_BBC_Text_To_Find_With_Different_Selectors()
    {
        // Arrange
        webDriver?.Navigate().GoToUrl(url);

        // Act
        try
        {
            if (webDriver != null)
            {
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                webDriver.SwitchTo().Frame(webDriver.FindElement(By.Id("sp_message_iframe_783538")));
                webDriver.FindElement(By.CssSelector(@"button[title=""I agree""]")).Click();
                webDriver.FindElement(By.CssSelector(@".ssrcss-1l58p0q-NavItemHoverState.e1gviwgp19")).Click();
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                var element = webDriver.FindElement(By.XPath(@"//*[@id=""homepage-link""]"));
            }
        }
        catch (System.Exception e)
        {
            // Assert
            System.Console.WriteLine(e.Message);
            Assert.True(false);
        }
        // Assert
        Assert.True(true);
    }

    [Test]
    public void Test_For_BBC_Text_To_Find_Only_With_XPath_Axes()
    {
        // Arrange
        webDriver?.Navigate().GoToUrl(url);

        // Act
        try
        {
            if (webDriver != null)
            {
                webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                webDriver.SwitchTo().Frame(webDriver.FindElement(By.XPath(@"//*[@id=""sp_message_iframe_783538""]")));
                webDriver.FindElement(By.XPath(@"//*[@id=""notice""]/div[3]/div[2]/button")).Click();
                webDriver.FindElement(By.XPath(@"//*[@id=""header-content""]/nav/div[1]/div/div[2]/ul[2]/li[5]/a/span")).Click();
                var element = webDriver.FindElement(By.XPath(@"//*[@id=""homepage-link""]"));
            }
        }
        catch (System.Exception e)
        {
            // Assert
            System.Console.WriteLine(e.Message);
            Assert.True(false);
        }
        // Assert
        Assert.True(true);
    }

    [Test]
    public void Test_For_Search_Bar_In_Order_To_Get_Football_Related_Results_Only_XPath_Axes()
    {
        if (webDriver != null)
        {
            // Arrange
            webDriver.Navigate().GoToUrl(url);

            // Act
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            webDriver.SwitchTo().Frame(webDriver.FindElement(By.XPath(@"//*[@id=""sp_message_iframe_783538""]")));
            webDriver.FindElement(By.XPath(@"//*[@id=""notice""]/div[3]/div[2]/button")).Click();
            webDriver.FindElement(By.XPath(@"//*[@id=""header-content""]/nav/div[1]/div/div[3]")).Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            var tmp = webDriver.FindElement(By.XPath(@"//*[@id=""searchInput""]"));
            tmp.Click();
            tmp.Clear();
            tmp.SendKeys("Football");
            webDriver.FindElement(By.XPath(@"//*[@id=""searchButton""]")).Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            webDriver.FindElement(By.XPath(@"/html/body/div/div/div/div[2]/div/main/div[4]/div/div/ul/li[1]/div/div/div[1]/div[1]/a"));
        }
        // Assert
        Assert.True(true);
    }

    [Test]
    public void Test_For_Search_Bar_In_Order_To_Get_Football_Related_Results_With_Different_Selectors()
    {
        if (webDriver != null)
        {
            // Arrange
            webDriver.Navigate().GoToUrl(url);

            // Act
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            webDriver.SwitchTo().Frame(webDriver.FindElement(By.Id("sp_message_iframe_783538")));
            webDriver.FindElement(By.CssSelector(@"button[title=""I agree""]")).Click();
            webDriver.FindElement(By.XPath(@"//*[@id=""header-content""]/nav/div[1]/div/div[3]")).Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            var tmp = webDriver.FindElement(By.Id("searchInput"));
            tmp.Click();
            tmp.Clear();
            tmp.SendKeys("Football");
            webDriver.FindElement(By.XPath(@"//*[@id=""searchButton""]")).Click();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            webDriver.FindElement(By.LinkText("Football"));
        }
        // Assert
        Assert.True(true);
    }
}